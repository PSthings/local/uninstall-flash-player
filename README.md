# Uninstall Flash Player

**Uninstalls all signs of flash player from Windows 10 machine**<br>
Tested on Win 10 build 1909 and 2004<br>

---

### How it works
This script:<br>
1. Downloads and runs [Adobe Flash removal tool](https://helpx.adobe.com/flash-player/kb/uninstall-flash-player-windows.html)<br>
2. Downloads and install Windows Update [KB4577586](https://support.microsoft.com/en-us/help/4577586/update-for-removal-of-adobe-flash-player) if build is 1909<br>
3. Uninstalls any Flash ActiveX or NPAPI plugins<br>
4. Removes:
    >`C:\Windows\system32\Macromed\Flash`<br>
    >`C:\Windows\SysWow64\Macromed\Flash`<br>
    >`%appdata%\Adobe\Flash Player`<br>
    >`%appdata%\Macromedia\Flash Player`<br>
    >`C:\Windows\SysWOW64\FlashPlayerApp.exe`<br>
    >`C:\Windows\SysWOW64\FlashPlayerCPLApp.cpl`<br>

### Installation
1. [Download](https://gitlab.com/PSthings/local/uninstall-flash-player/-/blob/master/flashuninstaller.ps1) `flashuninstaller.ps1`
2. Run Windows Powershell as administrator
3. Run `flashuninstaller.ps1` using the relative path where its downloaded. Ex:
```Powershell
.\flashuninstaller.ps1
```
If you encounter an error regarding running scripts being blocked (ExecutionPolicy):<br>
Run command
```powershell
Set-ExecutionPolicy Bypass -Force
```
Then try running `flashuninstaller.ps1` again in Step 3.

---

